<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Holiday Home for Ferienwohnung Venningen',
    'description' => 'Extension to manage the homepage for small holiday home',
    'author' => 'Moritz Kiehl',
    'author_email' => 'moritzkiehl@gmail.com',
    'state' => 'beta',
    'clearCacheOnLoad' => true,
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'MoritzKiehl\\HolidayHome\\' => 'Classes'
        ],
    ],
];